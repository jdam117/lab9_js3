"use strict";

var text = "This is some text.";
text = text.toUpperCase();
var frequencies = [];

for (var i = 0; i < text.length; i++) {

    var character = text.charAt(i);
    // TODO If the current character already has an entry in the array (i.e. frequencies[character] != undefined),
    // increment that entry. Otherwise, add a new entry with the initial value of 1.
     if (frequencies[character] == undefined){
         frequencies[character] = 1;
     }else{
         frequencies[character] += 1;
    }

}
console.log(frequencies);
for(var property in frequencies){
    var value = frequencies[property];
    console.log(property + " is: " + "and value is : " + value);
    
}

// TODO Print out the result
console.log("Text: " + text);
