"use strict";

// TODO Car
var car = {
    Year: "2007",
    Make: "BMW",
    Model:"323i",
    Body_Type:"Sedan",
    Transmission: "Tiptronic",
    Odometer: "68,512",
    Price: "$16,000"
}

// TODO Music

var Music = {
    Album: "1989",
    Artist: "Taylor Swift",
    Year: 2014,
    Genre: "pop",
    Tracks: ["Welcome to New York", "Blank Space", "Style", "Out of the Woods", "All You Had to Do Was Stay", "Shake It Off", "I Wish You Would",
        "Bad Blood", "Wildest Dreams", "How You Get the Girl", "This Love", "I Know Places", "Clean"],
    toString: function() {
        return "Album " + this.Album + " Was released in " + this.Year;
    }
}
console.log(Music.toString());

