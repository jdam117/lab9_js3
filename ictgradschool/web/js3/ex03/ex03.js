"use strict";

// TODO Create object prototypes here
function MusicAlbum(album, artist, year, genre, tracks) {
    this.album = album;
    this.artist = artist;
    this.year = year;
    this.genre = genre;
    this.tracks = tracks;
    this.toString = function () {
        return "Album " + this.album + " was released in " + this.year;
    }
}
// TODO Create functions here

function getAlbums(MusicAlbum1, MusicAlbum2, MusicAlbum3) {
    var allAlbums = [MusicAlbum1, MusicAlbum2, MusicAlbum3];
    return allAlbums;
}

function printAlbums(getAlbums) {
    console.log("Albums: ")
    for (var i = 0; i < getAlbums.length; i++) {
        console.log(getAlbums[i].toString());
        var trackLength =getAlbums[i].tracks.length;
        for(var count = 0; count < trackLength; count++){
            console.log(getAlbums[i].tracks[count]);
        }
    }
}

// TODO Complete the program here

var album1 = new MusicAlbum("1989_1", "Taylor Swift", 2014, "pop", ["Welcome to New York", "Blank Space", "Style", "Out of the Woods", "All You Had to Do Was Stay", "Shake It Off", "I Wish You Would",
    "Bad Blood", "Wildest Dreams", "How You Get the Girl", "This Love", "I Know Places", "Clean"]);
var album2 = new MusicAlbum("1989_2", "Taylor Swift", 2014, "pop", ["Welcome to New York", "Blank Space", "Style", "Out of the Woods", "All You Had to Do Was Stay", "Shake It Off", "I Wish You Would",
    "Bad Blood", "Wildest Dreams", "How You Get the Girl", "This Love", "I Know Places", "Clean"]);
var album3 = new MusicAlbum("1989_3", "Taylor Swift", 2014, "pop", ["Welcome to New York", "Blank Space", "Style", "Out of the Woods", "All You Had to Do Was Stay", "Shake It Off", "I Wish You Would",
    "Bad Blood", "Wildest Dreams", "How You Get the Girl", "This Love", "I Know Places", "Clean"]);


printAlbums(getAlbums(album1,album2,album3));